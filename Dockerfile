FROM nvidia/cuda:8.0-cudnn5-devel-ubuntu16.04

MAINTAINER Tamas Jozsa <tamas@machinelearningpros.com>

USER root

# Install all OS dependencies for notebook server that starts but lacks all
# features (e.g., download as all possible file formats)
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -yq dist-upgrade --allow-unauthenticated \
 && apt-get install -yq --no-install-recommends --allow-unauthenticated \
    wget \
    bzip2 \
    ca-certificates \
    sudo \
    locales \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

# Install Tini
RUN wget --quiet https://github.com/krallin/tini/releases/download/v0.10.0/tini && \
    echo "1361527f39190a7338a0b434bd8c88ff7233ce7b9a4876f3315c22fce7eca1b0 *tini" | sha256sum -c - && \
    mv tini /usr/local/bin/tini && \
    chmod +x /usr/local/bin/tini

# Configure environment
ENV SHELL /bin/bash
ENV NB_USER jovyan
ENV NB_UID 1000
ENV HOME /home/$NB_USER
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

# Create jovyan user with UID=1000 and in the 'users' group
RUN useradd -m -s /bin/bash -N -u $NB_UID $NB_USER

USER $NB_USER

# Setup jovyan home directory
RUN mkdir /home/$NB_USER/work && \
    echo "cacert=/etc/ssl/certs/ca-certificates.crt" > /home/$NB_USER/.curlrc

USER root

EXPOSE 8888
WORKDIR /home/$NB_USER/work

# Configure container startup


# Switch back to jovyan to avoid accidental container runs as root
USER $NB_USER

USER root
# Install all OS dependencies for fully functional notebook server
RUN apt-get update && apt-get install -yq --no-install-recommends \
    git \
    build-essential \
    python-dev \
    python3-pip \
    unzip \
    libsm6 \
    pandoc \
    && apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN pip3 install --upgrade pip

RUN pip3 install -U setuptools

COPY requirements $workdir

RUN pip3 install -r requirements.txt

RUN pip3 install tensorflow-gpu 

RUN pip3 install Keras

# Switch back to jovyan to avoid accidental container runs as root
USER $NB_USER

ENV logs /home/$NB_USER/work/logs

# Add local files as late as possible to avoid cache busting
# COPY model $workdir
# COPY start.sh $workdir
# RUN mkdir -p ~/.keras/datasets
# RUN mkdir -p $logs

# COPY cifar-10-python.tar.gz /home/jovyan/.keras/datasets/cifar-10-batches-py.tar.gz

# CMD sh start.sh
